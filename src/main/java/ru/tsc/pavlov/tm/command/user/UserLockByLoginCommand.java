package ru.tsc.pavlov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.exception.system.AccessDeniedException;
import ru.tsc.pavlov.tm.model.User;
import ru.tsc.pavlov.tm.util.TerminalUtil;

public final class UserLockByLoginCommand extends AbstractUserCommand{

    @NotNull
    @Override
    public String getName() {
        return "user-lock-by-login";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Lock user by login";
    }

    @Override
    public void execute() {
        System.out.println("PLEASE ENTER USER LOGIN:");
        final String login = TerminalUtil.nextLine();
        @Nullable final String id = serviceLocator.getUserService().findByLogin(login).getId();
        @Nullable final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        if (id.equals(currentUserId)) throw new AccessDeniedException();
        serviceLocator.getUserService().lockUserByLogin(login);
        System.out.println("USER SUCCESSFULLY LOCKED");
    }

}
