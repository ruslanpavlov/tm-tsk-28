package ru.tsc.pavlov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.enumerated.UserRole;
import ru.tsc.pavlov.tm.util.TerminalUtil;

public class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @Override
    public UserRole[] roles() {
        return UserRole.values();
    }

    @NotNull
    @Override
    public String getName() {
        return TerminalConst.PROJECT_UPDATE_BY_ID;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update project by id";
    }

    @Override
    public void execute() {
        @Nullable final String userId = getAuthService().getCurrentUserId();

        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        if (!getProjectService().existsById(id)) {
            System.out.println("Incorrect values");
            return;
        }
        System.out.println("[ENTER NAME]");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        @Nullable final String description = TerminalUtil.nextLine();
        getProjectService().updateById(userId, id, name, description);
        System.out.println("[OK]");
    }

}
