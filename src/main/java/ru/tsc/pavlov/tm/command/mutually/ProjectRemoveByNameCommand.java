package ru.tsc.pavlov.tm.command.mutually;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.enumerated.UserRole;
import ru.tsc.pavlov.tm.exception.empty.EmptyNameException;
import ru.tsc.pavlov.tm.util.TerminalUtil;

public class ProjectRemoveByNameCommand extends AbstractMutuallyCommand {

    @Nullable
    @Override
    public UserRole[] roles() {
        return UserRole.values();
    }

    @NotNull
    @Override
    public String getName() {
        return TerminalConst.PROJECT_REMOVE_BY_NAME;
    }


    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove project by name";
    }

    @Override
    public void execute() {
        @Nullable final String userId = getAuthService().getCurrentUserId();
        System.out.println("[ENTER PROJECT NAME]");
        @Nullable final String name = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        getProjectTaskService().removeByName(userId, name);
        System.out.println("[OK]");
    }

}
