package ru.tsc.pavlov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.enumerated.UserRole;
import ru.tsc.pavlov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.pavlov.tm.model.Project;
import ru.tsc.pavlov.tm.util.TerminalUtil;

public class ProjectFinishByIndexCommand extends AbstractProjectCommand {

    @Override
    public UserRole[] roles() {
        return UserRole.values();
    }

    @NotNull
    @Override
    public String getName() {
        return TerminalConst.PROJECT_FINISH_BY_INDEX;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Finish project by index";
    }

    @Override
    public void execute() {
        @Nullable final String userId = getAuthService().getCurrentUserId();
        System.out.println("[ENTER INDEX]");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final Project project = getProjectService().finishByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
    }

}
