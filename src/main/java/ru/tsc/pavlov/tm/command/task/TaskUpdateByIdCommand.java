package ru.tsc.pavlov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.enumerated.UserRole;
import ru.tsc.pavlov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.pavlov.tm.util.TerminalUtil;

public class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @Override
    public UserRole[] roles() {
        return UserRole.values();
    }

    @NotNull
    @Override
    public String getName() {
        return TerminalConst.TASK_UPDATE_BY_ID;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update project by id";
    }

    @Override
    public void execute() {
        @Nullable final String userId = getAuthService().getCurrentUserId();
        System.out.println("[ENTER ID]");
        @Nullable final String id = TerminalUtil.nextLine();
        if (!getTaskService().existsById(id)) throw new TaskNotFoundException();
        System.out.println("[ENTER NAME]");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        @Nullable final String description = TerminalUtil.nextLine();
        getTaskService().updateById(userId, id, name, description);
        System.out.println("[OK]");
    }

}
