package ru.tsc.pavlov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.api.service.ServiceLocator;
import ru.tsc.pavlov.tm.enumerated.UserRole;
import ru.tsc.pavlov.tm.util.StringUtil;

public abstract class AbstractCommand {

    @Nullable
    protected ServiceLocator serviceLocator;

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    public abstract String getName();

    @Nullable
    public abstract String getArgument();

    @Nullable
    public abstract String getDescription();

    public abstract void execute();

    public UserRole[] roles() {
        return null;
    }

    @NotNull
    public final String toString() {
        String result = "";
        String name = getName();
        String arg = getArgument();
        String description = getDescription();

        if (!StringUtil.isEmpty(name)) result += name;
        if (!StringUtil.isEmpty(arg)) result += " " + arg + " ";
        if (!StringUtil.isEmpty(description)) result += " - " + description;
        return result;
    }

}
