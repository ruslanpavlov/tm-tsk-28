package ru.tsc.pavlov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.dto.Domain;
import ru.tsc.pavlov.tm.enumerated.UserRole;

import java.io.FileInputStream;
import java.io.ObjectInputStream;


public class DataBinaryLoadCommand extends AbstractDataCommand{
    @NotNull
    public UserRole[] roles() {
        return new UserRole[]{UserRole.ADMIN};
    }

    @NotNull
    @Override
    public String getName() {
        return "data-bin-load";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load binary data.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_BINARY);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
        objectInputStream.close();
        fileInputStream.close();
    }

}
