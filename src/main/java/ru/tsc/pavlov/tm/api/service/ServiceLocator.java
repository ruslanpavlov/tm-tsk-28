package ru.tsc.pavlov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.pavlov.tm.api.service.IPropertyService;

public interface ServiceLocator {

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IPropertyService getPropertyService();

}
