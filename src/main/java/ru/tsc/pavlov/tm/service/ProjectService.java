package ru.tsc.pavlov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.api.repository.IProjectRepository;
import ru.tsc.pavlov.tm.api.service.IProjectService;
import ru.tsc.pavlov.tm.enumerated.Status;
import ru.tsc.pavlov.tm.exception.empty.*;
import ru.tsc.pavlov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.pavlov.tm.model.Project;

public final class ProjectService extends AbstractOwnerService<Project> implements IProjectService {

    @NotNull private final IProjectRepository projectRepository;

    public ProjectService(@NotNull final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(@Nullable final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        projectRepository.add(project);
    }

    @Override
    public void create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException(name);
        @Nullable final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
    }

    @NotNull
    @Override
    public Project findByName(@Nullable final String userId,@Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findByName(userId, name);
    }

    @Override
    public Project findById(@Nullable final String userId,@Nullable final String id) {
        return projectRepository.findById(userId, id);
    }

    @Override
    public Project findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return projectRepository.findByIndex(userId, index);
    }

    @Override
    public void remove(String userId, Project entity) {

    }

    @Override
    public Integer getSize(String userId) {
        return null;
    }

    @Override
    public Project updateById(
            final String userId, final String id, final String name, final String description
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final Project project = projectRepository.findById(id);
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(
            final String userId, final Integer index, final String name, @NotNull final String description
    ) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final Project project = projectRepository.findByIndex(userId, index);
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public boolean existsByIndex(@NotNull final String userId, @NotNull final int index) {
        return projectRepository.existsByIndex(userId, index);
    }

    @Override
    public Project startById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.startById(userId, id);
    }

    @Override
    public Project startByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return projectRepository.startByIndex(userId, index);
    }

    @Override
    public Project startByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.startByName(userId, name);
    }

    @Override
    public Project finishById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.finishById(userId, id);
    }

    @Override
    public Project finishByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (projectRepository.getSize() < index - 1) throw new ProjectNotFoundException();
        return projectRepository.finishByIndex(userId, index);
    }

    @Override
    public Project finishByName(@Nullable final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.finishByName(userId, name);
    }

    @Override
    public Project changeStatusById(@Nullable final String userId, @Nullable final String id, Status status) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) status = Status.valueOf("IN PROGRESS");
        return projectRepository.changeStatusById(userId, id, status);
    }

    @Override
    public Project changeStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable Status status) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (status == null) status = Status.valueOf("IN PROGRESS");
        return projectRepository.changeStatusByIndex(userId, index, status);
    }

    @Override
    public Project changeStatusByName(@Nullable final String userId, @Nullable final String name, @Nullable Status status) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) status = Status.valueOf("IN PROGRESS");
        return projectRepository.changeStatusByName(userId, name, status);
    }



}
