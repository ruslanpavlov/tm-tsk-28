package ru.tsc.pavlov.tm.exception.empty;

import ru.tsc.pavlov.tm.exception.AbstractException;

public class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Error: Id is empty.");
    }

}
