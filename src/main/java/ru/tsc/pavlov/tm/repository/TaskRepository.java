package ru.tsc.pavlov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.api.repository.ITaskRepository;
import ru.tsc.pavlov.tm.enumerated.Status;
import ru.tsc.pavlov.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    @Override
    public boolean existsByIndex(@NotNull final String userId, @NotNull final int index) {
        return index < list.size() && index >= 0;
    }

    @Override
    public boolean existsByName(@NotNull final String userId, @NotNull final String name) {
        return findByName(userId, name) != null;
    }

    @Override
    public Task findByName(@NotNull final String userId, @NotNull final String name) {
        return findAll(userId).stream()
                .filter(t -> t.getName().equals(name))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public Task findByIndex(@NotNull final String userId, @NotNull final int index) {
        return list.get(index);
    }

    @Override
    public Task removeByName(@NotNull final String userId, @NotNull final String name) {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        list.remove(task);
        return task;
    }

    @Override
    public Task removeByIndex(@NotNull final String userId, @NotNull final int index) {
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        list.remove(task);
        return task;
    }

    @Override
    public Task startById(@NotNull final String userId, @NotNull final String id) {
        final Task task = findById(id);
        if (task == null)
            return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByIndex(@NotNull final String userId, @NotNull final Integer index) {
        final Task task = findByIndex(userId, index);
        if (task == null)
            return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByName(@NotNull final String userId, @NotNull final String name) {
        final Task task = findByName(userId, name);
        if (task == null)
            return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishById(@NotNull final String userId, @NotNull final String id) {
        final Task task = findById(id);
        if (task == null)
            return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByIndex(@NotNull final String userId, @NotNull final Integer index) {
        final Task task = findByIndex(userId, index);
        if (task == null)
            return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByName(@NotNull final String userId, @NotNull final String name) {
        final Task task = findByName(userId, name);
        if (task == null)
            return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task changeStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) {
        final Task task = findById(id);
        if (task == null)
            return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final Status status) {
        final Task task = findByIndex(userId, index);
        if (task == null)
            return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByName(@NotNull final String userId, @NotNull final String name, @NotNull final Status status) {
        final Task task = findByName(userId, name);
        if (task == null)
            return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task bindTaskToProjectById(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) {
        final Task task = findById(taskId);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskById(@NotNull final String userId, @NotNull final String taskId) {
        final Task task = findById(taskId);
        task.setProjectId(null);
        return task;
    }

    @Override
    public void removeAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        findAll(userId).stream()
                .filter(t -> projectId.equals(t.getProjectId()))
                .forEach(t -> t.setProjectId(null));
    }

    @NotNull
    @Override
    public List<Task> findAllTaskByProjectId(@NotNull final String userId, @NotNull String projectId) {
        return findAll(userId).stream()
                .filter(t -> t.getProjectId().equals(projectId))
                .collect(Collectors.toList());
    }

}
