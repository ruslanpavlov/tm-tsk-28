package ru.tsc.pavlov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.pavlov.tm.api.repository.ICommandRepository;
import ru.tsc.pavlov.tm.command.AbstractCommand;

import java.util.*;

public class CommandRepository implements ICommandRepository {

    @NotNull private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();
    @NotNull private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    @Override
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @NotNull
    @Override
    public Collection<String> getCommandNames() {
        return new ArrayList<>(commands.keySet());
    }

    @NotNull
    @Override
    public Collection<String> getCommandArg() {
        return new ArrayList<>(arguments.keySet());
    }

    @NotNull
    @Override
    public AbstractCommand getCommandByName(@NotNull final String name) {
        return commands.get(name);
    }

    @NotNull
    @Override
    public AbstractCommand getCommandByArg(@Nullable final String arg) {
        return arguments.get(arg);
    }

    @Override
    public void add(@NotNull final AbstractCommand command) {
        @Nullable final String arg = command.getArgument();
        @NotNull final String name = command.getName();
        if (Optional.ofNullable(arg).isPresent()) arguments.put(arg, command);
        if (Optional.ofNullable(name).isPresent()) commands.put(name, command);
    }

}
